
function sliderOnResize() {

    var object2 = $('.js-resize-slider');
    if (object2.length) {
        if ( window.innerWidth <= 600) {
            object2.slick({
                slidesToShow: 1,
                dots: true,
                arrows: true,
                nextArrow:'<button type="button" class="button button--bordered button--arrow forward"><span></span></button>',
                prevArrow:'<button type="button" class="button button--bordered button--arrow"><span></span></button>',

            });
        } else {
            $('.js-resize-slider.slick-initialized').slick('unslick');
        }
    }

}
function jsMainslider(){
    var slider = $('.js-mainslider');
    if (slider.length){
        slider.slick({
            arrows:false,
            dots:true,
            autoplay: true,
            autoplaySpeed: 5000,
            pauseOnHover:false
        });
    }
    var slider2 = $('.js-app-slider');
    if (slider2.length){
        slider2.slick({
            arrows:false,
            dots:true,
             fade: true,
            adaptiveHeight:true
        });
    }
    var slider3 = $('.js-logo-slider');
    var slider4 = $('.js-comment-slider');
    if (slider3.length){
        slider3.slick({
            arrows:true,
            dots:false,
            nextArrow:'<button type="button" class="button button--bordered button--arrow forward"><span></span></button>',
            prevArrow:'<button type="button" class="button button--bordered button--arrow"><span></span></button>',
            slidesToShow:3,
            centerMode: true,
            asNavFor: '.js-comment-slider',
            responsive: [
                { breakpoint: 992, settings: {  slidesToShow: 1, centerMode: false,}},
            ]
        });
        slider4.slick({
            arrows:false,
            dots:false,
            fade: true,
            adaptiveHeight:true,
            asNavFor: '.js-logo-slider'
        });
    }
    var slider5 = $('.js-logo-slider2');
    if (slider5.length){
        slider5.slick({
            arrows:true,
            dots:false,
            nextArrow:'<button type="button" class="button button--bordered button--arrow forward"><span></span></button>',
            prevArrow:'<button type="button" class="button button--bordered button--arrow"><span></span></button>',
            slidesToShow:4,
            responsive: [
                { breakpoint: 600, settings: {  slidesToShow: 1, }},
            ]
        });
    }
    var slider5 = $('.js-appslider');
    if (slider5.length){
        slider5.slick({
            arrows:true,
            dots:false,
            nextArrow:'<button type="button" class="button button--bordered button--arrow forward"><span></span></button>',
            prevArrow:'<button type="button" class="button button--bordered button--arrow"><span></span></button>',
            slidesToShow:4,
            responsive: [
                { breakpoint: 800, settings: {  slidesToShow: 3, }},
                { breakpoint: 600, settings: {  slidesToShow: 2, }},
                { breakpoint: 400, settings: {  slidesToShow: 1, }},
            ]
        });
    }
    var slider6 =   $('.js-pentaslider');
    if (slider6.length){
        slider6.slick({
            dots: false,
            arrows: true,
            centerMode: true,
            slidesToShow: 3,
            nextArrow:'<button type="button" class="button button--bordered button--arrow forward"><span></span></button>',
            prevArrow:'<button type="button" class="button button--bordered button--arrow"><span></span></button>',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        centerMode: false,
                        slidesToShow: 1
                    }
                }
            ]

        });
    }
    var slider71 = $('.js-slider-big');
    var slider72 = $('.js-slider-nav');
    if (slider71.length){
        slider71.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            asNavFor: '.js-slider-nav',
            vertical: true,
            verticalSwiping:true,
            responsive: [
                { breakpoint: 992, settings: {  vertical: false,  verticalSwiping:false}},
            ]
        });
        slider72.slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.js-slider-big',
            dots: false,
            vertical: true,
            verticalSwiping:true
        });
        var next = '.js-slider-big-next';
        var prev = '.js-slider-big-prev';
        if($(next).length){
            $(document).on('click', next, function () {
                slider71.slick('slickNext');
            });
        }else{
            $(document).off('click', next);
        }
        if($(prev).length){
            $(document).on('click', prev, function () {
                slider71.slick('slickPrev');
            });
        }else{
            $(document).off('click', prev);
        }
    }

}
function jsSelect() {
    var sel = $('.js-select-lang');
    if(sel.length){
        sel.styler({selectSmartPositioning:false});
    }
}
function relativeMenus(){
    if($('.js-rel-btn').length){
        $(document).on('click','.js-rel-btn', function(){
            scrolTop = document.documentElement.scrollTop;
            var rel = $(this).attr('data-rel');
            closeRel();
            openRel(rel);
        });
    }else{
        $(document).off('click','.js-rel-btn');
    }
    if($('.js-rel-close').length){
        $(document).on('click','.js-rel-close', closeRel);
    }else{
        $(document).off('click','.js-rel-close');
    }


    $(document).keyup(function(e) {
        if (e.keyCode === 27 && $('.js-rel-box').hasClass('active'))  closeRel()
    });
    var scrolTop = 0;
    function openRel(rel) {
        $('.js-rel-box[data-rel='+rel+']').addClass('active');
        $('body').addClass('relmenu-opened');
    }
    function closeRel() {
        $('.js-rel-box').removeClass('active');
        $('body').removeClass('relmenu-opened');
        document.documentElement.scrollTop = scrolTop;
    }
}
function inputFiles() {
    var btn1 = $('.inputfile');
    if(btn1.length){
        $(document).on( 'change', '.inputfile', function(e) {
            var fileName = '';
            var doned = $(this).closest('.order__files').find('.order__uploaded');
            var empty  = '<input type="file" name="user_file[]"  class="inputfile">';
            var loaded =  document.createElement('div');
            loaded.classList.add('order__one-file');

            if( this.files && this.files.length >= 1 ){
                fileName = e.target.value.split( '\\' ).pop();
                loaded.innerHTML = '<svg><use xlink:href="#minus"></use></svg><span>'+fileName+'</span>';
                loaded.appendChild(this);
                doned.append(loaded);
                $('.order__upload label').append(empty);
            }
        });
    }else{
        $(document).off( 'change','.inputfile');
    }

    $(document).on('click', '.order__one-file', function () {
        $(this).remove();
    });
}
function googleMap(elem){
        var myLatlng = new google.maps.LatLng(elem.data('lat'), elem.data('lng'));
        var myOptions = {
            zoom: 15,
            center: myLatlng,
            disableDefaultUI: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_BOTTOM
            },
            styles:[
                {
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#212121"
                        }
                    ]
                },
                {
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#757575"
                        }
                    ]
                },
                {
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "color": "#212121"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#757575"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "visibility": "off"
                        },
                        {
                            "weight": 1.5
                        }
                    ]
                },
                {
                    "featureType": "administrative.country",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#9e9e9e"
                        }
                    ]
                },
                {
                    "featureType": "administrative.locality",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#bdbdbd"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#757575"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#181818"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#616161"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "color": "#1b1b1b"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#2c2c2c"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#8a8a8a"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#373737"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#3c3c3c"
                        }
                    ]
                },
                {
                    "featureType": "road.highway.controlled_access",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#4e4e4e"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#616161"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#757575"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#3d3d3d"
                        }
                    ]
                }
            ]
        }
        var map = new google.maps.Map(document.getElementById('mapInit'), myOptions);
        var contentString = '<div class="marker-test">'+ poiter_desc +'</div>';
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            animation: google.maps.Animation.DROP,
             icon: 'images/marker.png'
        });

        marker.addListener('click', toggleBounce);
        function toggleBounce() {
            if (marker.getAnimation() !== null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        }

        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map,marker);
        });


}
function goTo(){
    if($('a.anchor-btn').length){
        $(document).on('click', 'a.anchor-btn', function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            var target = $(href).offset().top-65;
            $('html, body').animate({scrollTop:target},500);
        });
    }else{
        $(document).off('click', 'a.anchor-btn');
    }
}
function showMore() {
    var btn = '.js-showmore-btn';
    if($(btn).length){
        $(document).on('click', btn, function (evt) {
            evt.preventDefault();
            var box = $(this).closest('.js-showmore-box');
            var hid = box.find('.js-showmore-hid');
            hid.slideDown();
            $(this).remove();
        })
    }else{
        $(document).off('click', btn);
    }
}
function pocketSize() {
    var item = $('.pocket');
    if (item != undefined) {
        for (var i = 0; i < item.length; i++) {
            var inside = item.eq(i).find('.pocket__inside');
            var holder = item.eq(i).find('.pocket__holder');
            var button = item.eq(i).find('.js-pocket-show');
            var gradient = item.eq(i).find('.js-pocket-grad');
            if (inside.height() < holder.outerHeight()) {
                button.removeClass('hidden');
                gradient.removeClass('hidden');
            } else {
                button.addClass('hidden');
                gradient.addClass('hidden');
            }
        }
    }
}

function openPocket() {
    var button = $('.js-pocket-show');
    if (button.length > 0) {
        $(document).on('click', '.js-pocket-show', function () {
            var that = $(this);
            if (that.hasClass('opened')) {
                that.removeClass('opened');
                that.parents('.pocket__inside').find('.pocket__items').removeClass('show');
                setTimeout(function () {

                    that.parents('.pocket__inside').removeClass('opened');
                    that.parents('.pocket__inside').find('.pocket__gradient').removeClass('hidden');
                    that.parents('.pocket__inside').find('.pocket__gradient').removeClass('hide');
                    that.parents('.pocket').removeClass('opened');
                }, 500);
            } else {
                that.parents('.pocket').addClass('opened');
                that.addClass('opened');
                that.parents('.pocket__inside').addClass('opened');
                that.parents('.pocket__inside').find('.pocket__gradient').addClass('hide');
                that.parents('.pocket__inside').find('.pocket__items').addClass('show');
            }
        })
    }
}

function checkPockets() {
    $(window).resize(function () {
        if ($(window).width() < 1600) {
            pocketSize();
        }
    })
}


$(document).ready(function(){
    checkPockets();
    openPocket();
    pocketSize();
    goTo();
    showMore();
    jsMainslider();
    jsSelect();
    relativeMenus();
    inputFiles();
    sliderOnResize();
    googleMap($('.footer__addr-link').eq(0));


});
var onScroll = debounce(function() {
    if($(window).scrollTop() > 100){
        document.getElementsByTagName('header')[0].classList.add('white')
    }else{
        document.getElementsByTagName('header')[0].classList.remove('white');
    }
}, 150);
var onResize = debounce(function() {
    sliderOnResize();
}, 250);
window.addEventListener('resize', onResize);
window.addEventListener('scroll', onScroll);

function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};
(function(window, document) {
    'use strict';
    var file = 'images/sprite.svg'; // путь к файлу спрайта на сервере

    if (!document.createElementNS || !document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect) return true;
    var isLocalStorage = 'localStorage' in window && window['localStorage'] !== null,
        request,
        data,
        insertIT = function() {
            document.body.insertAdjacentHTML('afterbegin', data);
        },
        insert = function() {
            if (document.body) insertIT();
            else document.addEventListener('DOMContentLoaded', insertIT);
        };
    if (isLocalStorage && localStorage.getItem('inlineSVGrev') == revision) {
        data = localStorage.getItem('inlineSVGdata');
        if (data) {
            insert();
            return true;
        }
    }
    try {
        request = new XMLHttpRequest();
        request.open('GET', file, true);
        request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
                data = request.responseText;
                insert();
                if (isLocalStorage) {
                    localStorage.setItem('inlineSVGdata', data);
                    localStorage.setItem('inlineSVGrev', revision);
                }

            }
        }
        request.send();
    } catch (e) {}
}(window, document));
